const io = require('socket.io-client');
const Stove = require('./stove');

module.exports = class Controller {
  constructor(url = 'http://mia-cucina.herokuapp.com') {
    this.url = url;
    this._socket = io(url);
    this.stove = new Stove();

    this._socket.on('status', status => this._statusChange(status));
    this._socket.on('error', err => this._connectionError(err));
    this._socket.on('connect', () => this._showConnection(true));
    this._socket.on('disconnect', () => this._showConnection(false));
    this.stove.turnOff();
  }

  _statusChange(status) {
    if (status.isOn)
      this.stove.turnOn(status.temperature);
    else
      this.stove.turnOff();
  }

  _showConnection(conection) {
    console.log(`La estufa se ${conection ? 'conecto a' : 'ha desconectado de'} internet a las ${new Date()}`);
  }

  _connectionError(err) {
    console.error(err);
  }
}
