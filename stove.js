const Cylon = require('cylon');

const connection = { name: 'raspi', adaptor: 'raspi' };
const devices = [
  { name: 'flow1', driver: 'direct-pin', pin: 7 },
  { name: 'flow2', driver: 'direct-pin', pin: 11 },
  { name: 'gnd', driver: 'direct-pin', pin: 15 }
];

module.exports = class Stove {
  constructor() {
    this.isLoad = false;
    this._values = {};
    this._hardware = null;
    Cylon.robot({ connection, devices, work : hardware => this._onLoad(hardware) }).start();
  }

  turnOn(temperature) {
    if (!this.isLoad)
      return;
    
    this.turnOff();
    switch(temperature) {
      case 'high':
        this._setDeviceValue('flow2', 0);
        break;
      case 'low':
        this._setDeviceValue('gnd', 0);
        break;
    }    
    
    this._setDeviceValue('flow1', 0);
  }

  turnOff() {
    if (!this.isLoad)
      return;
    
    this._setDeviceValue('flow1', 1);
    this._setDeviceValue('flow2', 1);
    this._setDeviceValue('gnd', 1);
  }

  _setDeviceValue(device, value) {
    if (this._values[device] === value)
      return;
    
    this._hardware[device].digitalWrite(value);
    this._values[device] = value;
  }

  _onLoad(hardware) {
    this._hardware = hardware;
    this._setDeviceValue('gnd', 1);
    this.isLoad = true;
  }
}
